using FlowFields.Components;
using TMPro;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Serialization;

public class AgentsCountUI : MonoBehaviour
{
    [FormerlySerializedAs("_agentsCount")] [SerializeField] private TextMeshProUGUI _agentsCountLabel;

    private EntityManager _entityManager;
    private EntityQuery _agentsQuery;
    private int _previousCount = 0;
    private void Start()
    {
        _entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var agentsQueryDesc = new EntityQueryDesc
        {
            All = new [] {ComponentType.ReadOnly<NavAgent>(), ComponentType.ReadOnly<Sector>(), ComponentType.ReadOnly<Translation>() }
        };


        _agentsQuery = _entityManager.CreateEntityQuery(agentsQueryDesc);
    }

    private void LateUpdate()
    {
        var agentsCount = _agentsQuery.CalculateEntityCount();
        if(agentsCount == _previousCount)
            return;
        _previousCount = agentsCount;
        _agentsCountLabel.text = agentsCount.ToString();
    }
}
