using FlowFields.Components;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace FlowFields.AuthoringComponents
{
    public class MapAuthoring:MonoBehaviour, IConvertGameObjectToEntity
    {
        [SerializeField]
        private GameObject[] Prefabs;
        [SerializeField]
        public bool test { get; set; }
        [SerializeField] private int width = 10;
        [SerializeField] private int height = 10;
        
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponentData(entity,new Map {width = width, height = height});
        }

        [ContextMenu("Generate random map")]
        private void GenerateRandomWithSwansAtEdge()
        {
            // Destroy current children
            int count = transform.childCount;
            for(int i = count -1 ; i >= 0; i--)
                DestroyImmediate(transform.GetChild(i).gameObject);
            
            // generate
            NativeArray<byte> prefabs = new NativeArray<byte>(width*height, Allocator.TempJob);
            NativeArray<byte> neighbours = new NativeArray<byte>(prefabs.Length, Allocator.TempJob);
            
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < width; y++)
                {
                    var prefab = 0;//x== 0 || x ==  width - 1 || y == 0 || y == height - 1 ? Prefabs.Length - 1 : 0;
                    if (x > 0 && x < width - 1 && y > 0 && y <  height - 1)
                    {

                        for (int i = 0; i < neighbours.Length; i++)
                            neighbours[i] = 0;

                        if (x > 0)
                            neighbours[prefabs[x - 1 + y * width]]++;
                        if (y > 0)
                        {
                            var row = (y - 1) * width;
                            if (x > 0)
                                neighbours[prefabs[x - 1 + row]]++;
                            neighbours[prefabs[x + row]]++;
                            if (x < width - 1)
                                neighbours[prefabs[x + 1 + row]]++;
                        }

                        int max = 0;
                        int neigbourPrefab = -1;

                        for (int i = 0; i < neighbours.Length; i++)
                            if (i != 0 && neighbours[i] > max)
                            {
                                max = neighbours[i];
                                neigbourPrefab = i;
                            }
                        
                        var test = Random.Range(0f, 1f);
                        prefab =  test > max * 0.1f ? Random.Range(1, Prefabs.Length) : neigbourPrefab;
                    }

                    var tile = Instantiate(Prefabs[prefab], transform);
                    tile.transform.position = new Vector3(x, 0,y);
                    tile.isStatic = true;

                }
            }
            
            prefabs.Dispose();
            neighbours.Dispose();
        }

        [ContextMenu("Generate square rooms map")]
        private void GenerateSquareRooms()
        {
            // Destroy current children
            int count = transform.childCount;
            for(int i = count -1 ; i >= 0; i--)
                DestroyImmediate(transform.GetChild(i).gameObject);

            // generate
            NativeArray<byte> prefabs = new NativeArray<byte>(width*height, Allocator.TempJob);
            NativeArray<byte> neighbours = new NativeArray<byte>(prefabs.Length, Allocator.TempJob);

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < width; y++)
                {
                    var prefab = 1;

                    if ((y % 5 == 0 && x % 10 == 0 && y % 10 != 0)
                             || (x % 5 == 0 && y % 10 == 0 && x % 10 != 0))
                    {
                        var test = Random.Range(0f, 1f);
                        prefab = test > 0.4f ? 1 : Prefabs.Length - 1;
                    }
                    else if (y % 10 == 0 || x % 10 == 0)
                        prefab = Prefabs.Length - 1;

                    if(x==5 && y % 5 ==0 || y==5 && x % 5 == 0 )
                        prefab = 0;
                    if(x==width - 5 && y % 5 ==0 || y==width - 5 && x % 5 == 0 )
                        prefab = 0;
                    if(x==0 || x == width - 1 || y == 0 || y == width -1)
                        prefab = Prefabs.Length - 1;
                    var tile = Instantiate(Prefabs[prefab], transform);
                    tile.transform.position = new Vector3(x, 0,y);
                    tile.isStatic = true;

                }
            }

            prefabs.Dispose();
            neighbours.Dispose();
        }

        [ContextMenu("Generate open map")]
        private void GenerateOpenMap()
        {
            // Destroy current children
            int count = transform.childCount;
            for(int i = count -1 ; i >= 0; i--)
                DestroyImmediate(transform.GetChild(i).gameObject);

            // generate
            NativeArray<byte> prefabs = new NativeArray<byte>(width*height, Allocator.TempJob);
            NativeArray<byte> neighbours = new NativeArray<byte>(prefabs.Length, Allocator.TempJob);

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < width; y++)
                {
                    var prefab = 1;

                    if(x==0 || x == width - 1 || y == 0 || y == width -1)
                        prefab = Prefabs.Length - 1;
                    var tile = Instantiate(Prefabs[prefab], transform);
                    tile.transform.position = new Vector3(x, 0,y);
                    tile.isStatic = true;

                }
            }

            prefabs.Dispose();
            neighbours.Dispose();
        }
    }
}