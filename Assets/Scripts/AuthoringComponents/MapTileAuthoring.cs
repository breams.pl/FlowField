using FlowFields.Components;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace FlowFields.AuthoringComponents
{
    public class MapTileAuthoring:MonoBehaviour,IConvertGameObjectToEntity
    {
        public byte moveCost;
        
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            var position = transform.position;
            var tile = new MapTile
            {
                moveCost = moveCost
            };

            var sector = new Sector {coordinates = new int2((int) position.x, (int) position.z)};
            dstManager.AddComponentData(entity, tile);
            dstManager.AddComponentData(entity, sector);

        }
    }
}