using FlowFields.Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace FlowFields.AuthoringComponents
{
    public class NavAgentAuthoring : MonoBehaviour, IConvertGameObjectToEntity
    {
        [SerializeField] private float radius = 0.15f;
        [SerializeField] private float moveSpeed = 0.5f;

        [SerializeField] private float moveSharpness = 3;
        [SerializeField] private float decollisionDamping = 2;
        [SerializeField] private float orientSharpness = 10;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponentData(entity, new NavAgent
            {
                moveSpeed = moveSpeed,
                moveSharpness = moveSharpness,
                decollisionDamping = decollisionDamping,
                orientSharpness = orientSharpness,
                obstacleDistanceFactor = (half) 1f
            });
        }
    }
}