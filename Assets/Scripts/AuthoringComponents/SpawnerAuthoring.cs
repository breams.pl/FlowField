using System.Collections.Generic;
using FlowFields.Components;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Serialization;

namespace FlowFields.AuthoringComponents
{
    public class SpawnerAuthoring : MonoBehaviour, IConvertGameObjectToEntity, IDeclareReferencedPrefabs
    {
        [SerializeField] private GameObject prefab = null;
        [SerializeField] private float timeToSpawn = 1f;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponentData(entity, new Spawner
            {
                prefab = conversionSystem.GetPrimaryEntity(prefab),
                timeToSpawn = timeToSpawn,
                time = timeToSpawn
            });
        }

        public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
        {
            referencedPrefabs.Add(prefab);
        }
    }
}