using Unity.Entities;

namespace FlowFields.Components
{
    [GenerateAuthoringComponent]
    public struct Animation:IComponentData
    {
        public byte AnimationId;
        public byte FrameIndex;
        public byte FramesCount;
        public float TimePerFrame;
        public float CurrentTime;

    }
}