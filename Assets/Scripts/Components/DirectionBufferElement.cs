using Unity.Entities;
using Unity.Mathematics;

namespace FlowFields.Components
{
    [InternalBufferCapacity(8)]
    public struct DirectionBufferElement: IBufferElementData
    {

        public int2 Value;

        // The following implicit conversions are optional, but can be convenient.
        public static implicit operator int2(DirectionBufferElement e)
        {
            return e.Value;
        }

        public static implicit operator DirectionBufferElement(int2 e)
        {
            return new DirectionBufferElement { Value = e};
        }
    }


}