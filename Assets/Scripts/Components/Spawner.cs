using Unity.Entities;

namespace FlowFields.Components
{
    public struct Spawner:IComponentData
    {
        public Entity prefab;
        public float timeToSpawn;
        public float time;
    }
}