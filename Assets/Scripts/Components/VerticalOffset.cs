using System;
using Unity.Entities;
using Unity.Rendering;

namespace FlowFields.Components
{
    [Serializable]
    [GenerateAuthoringComponent]
    [MaterialProperty("_VerticalOffset", MaterialPropertyFormat.Float)]
    public struct VerticalOffset:IComponentData
    {
        public float Value;
    }
}