using System;
using Unity.Collections;
using Unity.Mathematics;

namespace FlowFields.Data
{
    public struct AStarField:IDisposable
    {
        public NativeArray<PathNode> Nodes;
        private const float MOVE_STRAIGHT_COST = 1;
        private const float MOVE_DIAGONAL_COST = math.SQRT2;

        private float _HeuristicFactor;
        public AStarField(MapData mapData, float heuristicFactor = 1)
        {
            _HeuristicFactor = heuristicFactor;

            Nodes = new NativeArray<PathNode>(mapData.height*mapData.width, Allocator.Persistent);
            for (var i = 0; i < Nodes.Length; i++)
            {
                var node = new PathNode()
                {
                    index = i,
                    position = new int2(i % mapData.width,(int) math.floor((float) i / mapData.width)),
                    gCost = float.MaxValue,
                    hCost = float.MaxValue,
                    fCost = float.MaxValue,
                    DirectionCode = mapData.tiles[i].availableDirectionsCode,
                    cameFromNodeIndex = -1
                };
                Nodes[i] = node;
            }
        }
        public static float CalculateDistanceCost(int2 aPosition, int2 bPosition)
        {
            var xDistance = math.abs(aPosition.x - bPosition.x);
            var yDistance = math.abs(aPosition.y - bPosition.y);
            var remaining = math.abs(xDistance - yDistance);
            return (math.SQRT2 * math.min(xDistance, yDistance) + remaining);
        }
        public void Dispose()
        {
            Nodes.Dispose();
        }
    }

    public struct PathNode
    {
        public int2 position;
        public int index;
        public float gCost;
        public float hCost;
        public float fCost;

        public byte DirectionCode;
        public int cameFromNodeIndex;

        public void CalculateFCost() {
            fCost = gCost + hCost;
        }
    }
}