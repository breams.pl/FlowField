using System;
using UnityEngine;

namespace FlowFields.Data
{
    [CreateAssetMenu(menuName = "Data/Animation", fileName = "animation")]
    public class AnimationsData : ScriptableObject
    {
        public Animation[] Animations;

        [SerializeField] private string _PathToOutputMesh;

        public int AnimationOffset = 10000;
        public int FrameOffset = 100;

#if UNITY_EDITOR
        [ContextMenu("Generate Mesh")]
        private void GenerateMesh()
        {
            Mesh  mesh = new Mesh();
            Mesh  temp = new Mesh();
            var size = GetSize();
            var cbis = new CombineInstance[size];
            var cbiIndex = 0;
            for (var animationIndex = 0; animationIndex < Animations.Length; animationIndex++)
            {
                var animation = Animations[animationIndex];

                for (var frame = 0; frame < animation.Frames.Length; frame++)
                {
                    var source = animation.Frames[frame];
                    cbis[cbiIndex].mesh = source;
                    cbis[cbiIndex].transform = Matrix4x4.Translate(Vector3.up * (animationIndex*AnimationOffset + frame*FrameOffset));

                    cbiIndex++;
                }
            }
            mesh.CombineMeshes(cbis);
            mesh.name = this.name;
            var path =_PathToOutputMesh + name + ".asset";
            Debug.Log(path);
            UnityEditor.AssetDatabase.CreateAsset( mesh, path);
            UnityEditor.AssetDatabase.SaveAssets();
        }
#endif

        private int GetSize()
        {
            var size = 0;
            for (var animationIndex = 0; animationIndex < Animations.Length; animationIndex++)
            {
                var animation = Animations[animationIndex];
                for (var frame = 0; frame < animation.Frames.Length; frame++)
                    size++;
            }

            return size;
        }
        private void CopyMesh(Mesh dst, Mesh src)
        {
            dst.SetVertices(src.vertices);
            dst.SetNormals(src.normals);
            dst.SetUVs(0, src.uv);
            dst.SetColors(src.colors);
        }
    }

    [Serializable]
    public struct Animation
    {
        public string Name;
        public Mesh[] Frames;
        public float TimePerFrame;
    }
}