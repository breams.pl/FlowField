using System;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace Data
{
    [NativeContainerSupportsDeallocateOnJobCompletion]
    [NativeContainerSupportsMinMaxWriteRestriction]
    [NativeContainer]
    public unsafe struct NativeBinaryMinHeap:IDisposable
    {
        [NativeDisableUnsafePtrRestriction] private void* m_Buffer;
        private int m_capacity;
#if ENABLE_UNITY_COLLECTIONS_CHECKS
        private AtomicSafetyHandle m_Safety;
        [NativeSetClassTypeToNullOnSchedule] private DisposeSentinel m_DisposeSentinel;
#endif
        private int m_length;
        private Allocator m_AllocatorLabel;

        public NativeBinaryMinHeap(int capacity, Allocator allocator/*, NativeArrayOptions options = NativeArrayOptions.ClearMemory*/)
        {
            Allocate(capacity, allocator, out this);
            /*if ((options & NativeArrayOptions.ClearMemory) != NativeArrayOptions.ClearMemory)
                return;
            UnsafeUtility.MemClear(m_Buffer, (long) m_capacity * UnsafeUtility.SizeOf<MinHeapNode>());*/
        }
        private static void Allocate(int capacity, Allocator allocator, out NativeBinaryMinHeap nativeMinHeap)
        {
            var size = (long) UnsafeUtility.SizeOf<BinaryMinHeapNode>() * capacity;
            if (allocator <= Allocator.None)
                throw new ArgumentException("Allocator must be Temp, TempJob or Persistent", nameof (allocator));
            if (capacity < 0)
                throw new ArgumentOutOfRangeException(nameof (capacity), "Length must be >= 0");
            if (size > int.MaxValue)
                throw new ArgumentOutOfRangeException(nameof (capacity),
                    $"Length * sizeof(T) cannot exceed {(object) int.MaxValue} bytes");

            nativeMinHeap.m_Buffer = UnsafeUtility.Malloc(size, UnsafeUtility.AlignOf<BinaryMinHeapNode>(), allocator);
            nativeMinHeap.m_capacity = capacity;
            nativeMinHeap.m_AllocatorLabel = allocator;
            //nativeMinHeap.m_MinIndex = 0;
            //nativeMinHeap.m_MaxIndex = capacity - 1;
            //nativeMinHeap.m_head = -1;
            nativeMinHeap.m_length = 0;

#if ENABLE_UNITY_COLLECTIONS_CHECKS
            DisposeSentinel.Create(out nativeMinHeap.m_Safety, out nativeMinHeap.m_DisposeSentinel, 1, allocator);
#endif
        }

        public void Push(BinaryMinHeapNode node)
        {

        }

        public bool HasNext()
        {
#if ENABLE_UNITY_COLLECTIONS_CHECKS
            AtomicSafetyHandle.CheckReadAndThrow(m_Safety);
#endif
            return m_length > 0;
        }
        public void Clear()
        {
            m_length = 0;
        }
        public void Dispose()
        {
            if (!UnsafeUtility.IsValidAllocator(m_AllocatorLabel))
                throw new InvalidOperationException("The NativeArray can not be Disposed because it was not allocated with a valid allocator.");
#if ENABLE_UNITY_COLLECTIONS_CHECKS
            DisposeSentinel.Dispose(ref m_Safety, ref m_DisposeSentinel);
#endif
            UnsafeUtility.Free(m_Buffer, m_AllocatorLabel);
            m_Buffer = null;
            m_capacity = 0;
        }

    }

    public struct BinaryMinHeapNode
    {
        public float ExpectedCost;
    }
}