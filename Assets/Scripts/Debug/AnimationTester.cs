﻿using System;
using FlowFields.Data;
using UnityEngine;
using Animation = FlowFields.Data.Animation;

[ExecuteAlways]
public class AnimationTester : MonoBehaviour
{
    public AnimationsData Animations;

    public int id = 1;
    public int index = 0;
    public float time = 0;
    public Animation animation;
    private void OnEnable()
    {
        animation = Animations.Animations[id];
        time = 0;
        index = 0;
    }

    private void OnValidate()
    {
        animation = Animations.Animations[id];
        time = 0;
        index = 0;
    }

    // Update is called once per frame
    void Update()
    {

        if(Animations == null)
            return;

        time += Time.deltaTime;
        if (time > animation.TimePerFrame)
        {

            transform.localPosition = -Vector3.up * (id * Animations.AnimationOffset + index * Animations.FrameOffset);
            time -= animation.TimePerFrame;
            index++;
            if (index == animation.Frames.Length)
                index = 0;
        }
    }
}
