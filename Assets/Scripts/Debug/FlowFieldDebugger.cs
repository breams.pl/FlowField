using System;
using Unity.Entities;
using UnityEngine;
using FlowFields.Systems;
using Unity.Mathematics;

namespace UnityTemplateProjects.Debug
{
    public class FlowFieldDebugger : MonoBehaviour
    {
        public bool drawWals = false;
        public bool drawDirecrions = false;
        public bool drawFlowVector = true;
        private FlowFieldSystem flowFieldSystem;
        private MapGenerationSystem mapGenerationSystem;

        private void Start()
        {

            mapGenerationSystem = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<MapGenerationSystem>();
        }

        private void OnDrawGizmosSelected()
        {
            if(flowFieldSystem == null)
                flowFieldSystem = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<FlowFieldSystem>();
            if(mapGenerationSystem == null)
                mapGenerationSystem = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<MapGenerationSystem>();
            mapGenerationSystem.Update();
            flowFieldSystem.Update();
            flowFieldSystem.generateField.Complete();
            var width = mapGenerationSystem.mapData.width;
            var tiles = mapGenerationSystem.mapData.tiles;
            for (int i = 0; i < tiles.Length; i++)
            {
                var tile = tiles[i];
                var x = i % width;;
                var y = Mathf.FloorToInt((float)i / width);
                var pos = new Vector3(x, 0, y);
                var flowField = flowFieldSystem.flowField.flowField[x + y * width];
                var forward = new Vector3(flowField.x, 0, flowField.y);
                if(drawWals)
                    DrawWals(pos, tile.moveCost);
                if(drawDirecrions)
                    DrawGizmo2(pos, tile.availableDirectionsCode);
                if (forward.magnitude > 0 && drawFlowVector)
                {
                    var quaternion = Quaternion.LookRotation(forward, Vector3.up);
                    DrawGizmo(pos, quaternion, tile.moveCost+tile.agents*0.5f);
                }
                else
                {
                    DrawGizmo(pos);
                }
            }
        }

        private void DrawGizmo(Vector3 pos, Quaternion rotation, float movecost)
        {
            Gizmos.color = Color.black;
            pos += Vector3.up*3;
            Gizmos.DrawLine(pos, pos + rotation * (Vector3.left * 0.25f));
            Gizmos.DrawLine(pos, pos + rotation * (Vector3.right * 0.25f));
            Gizmos.DrawLine(pos, pos + rotation * (Vector3.forward * 0.5f/movecost));
        }

        private void DrawGizmo(Vector3 pos)
        {
            Gizmos.color = Color.black;
            pos += Vector3.up*3;
            Gizmos.DrawLine(pos, pos + Vector3.left * 0.25f);
            Gizmos.DrawLine(pos, pos + Vector3.right * 0.25f);
            Gizmos.DrawLine(pos, pos + Vector3.forward * 0.25f);
            Gizmos.DrawLine(pos, pos + Vector3.back * 0.25f);
        }
        private void DrawGizmo2(Vector3 pos, byte code)
        {
            Gizmos.color = Color.black;
            pos += Vector3.up*3;
            Gizmos.color = Color.red;
            for (var i = 0; i < 8; i++)
            {
                var dir = dirs[i];
                var hasWall = (code & (1 << i)) != 0;
                if (hasWall)
                    Gizmos.DrawLine(pos, pos + new Vector3(dir.x, 0, dir.y)*0.3f);
            }
        }

        private static int2[] dirs = new[]
        {
            new int2(0, 1),
            new int2(1, 1),
            new int2(1, 0),
            new int2(1, -1),
            new int2(0, -1),
            new int2(-1, -1),
            new int2(-1, 0),
            new int2(-1, 1)
        };
        private void DrawWals(Vector3 pos, byte cost)
        {
            Color color = Color.white;
            float height = 1;
            switch (cost)
            {
                case 1:
                    color = Color.blue;
                    height = 1;
                    break;
                case 2:
                    color = Color.cyan;
                    height = 1;
                    break;
                case 3:
                    color = Color.magenta;
                    height = 2;
                    break;
                case 4:
                    color = Color.green;
                    height = 2;
                    break;
                case 5:
                    color = Color.yellow;
                    height = 3;
                    break;
                case 6:
                    color = Color.red;
                    height = 3;
                    break;
                case 7:
                    color = Color.gray;
                    height = 9;
                    Gizmos.color = color;
                    Gizmos.DrawCube(pos, new Vector3(1,height/10,1));
                    break;
            }


            
        }
    }
}