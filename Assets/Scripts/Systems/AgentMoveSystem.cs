using FlowFields.Components;
using FlowFields.Data;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;
using Random = Unity.Mathematics.Random;

namespace FlowFields.Systems
{
   // [UpdateInGroup(typeof(InitializationSystemGroup))]
    public partial class AgentMoveSystem:SystemBase
    {
        private FlowFieldSystem flowFieldSystem;
        private MapGenerationSystem mapGenerationSystem;
        private Random random;
        protected override void OnCreate()
        {
            base.OnCreate();
            flowFieldSystem = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<FlowFieldSystem>();
            mapGenerationSystem = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<MapGenerationSystem>();
            random = new Random(42);
        }

        protected override void OnUpdate()
        {
            if (!flowFieldSystem.flowField.isValid)
                return;
            
            var moveJob = new MoveAgentsJob
            {
                mapData = mapGenerationSystem.mapData,
                
                flowField = flowFieldSystem.flowField,
                deltaTime = Time.DeltaTime,
                random = random
            };
            
            Dependency = moveJob.Schedule(JobHandle.CombineDependencies(Dependency, flowFieldSystem.generateField));
        }

        [BurstCompile]
        private partial struct MoveAgentsJob : IJobEntity
        {
            [ReadOnly]
            public MapData mapData;
            [ReadOnly]
            public FlowField flowField;
            public float deltaTime;
            public Random random;

            public void Execute(ref Translation translation, ref NavAgent agent, ref PhysicsVelocity velocity,
                ref Rotation rotation)
            {
                var pos = new float2(translation.Value.x, translation.Value.z);
                var tile = new int2((int) math.floor(translation.Value.x), (int) math.floor(translation.Value.z));
                var uv = pos - new float2(tile.x, tile.y);
                uv.x = 3f * uv.x * uv.x - 2f * uv.x * uv.x * uv.x;
                uv.y = 3f * uv.y * uv.y - 2f * uv.y * uv.y * uv.y;

                if (!mapData.IsInsideMap(tile) || !mapData.IsInsideMap(new int2(tile.x + 1, tile.y + 1)))
                    return;
                var a = flowField.GetFlowField(tile);
                var b = flowField.GetFlowField(tile + new int2(1, 0));

                if (!(math.lengthsq(a) > 0))
                    a = b;
                if (!(math.lengthsq(b) > 0))
                    b = a;
                var c = flowField.GetFlowField(tile + new int2(0, 1));
                var d = flowField.GetFlowField(tile + new int2(1, 1));

                if (!(math.lengthsq(c) > 0))
                    c = d;
                if (!(math.lengthsq(d) > 0))
                    d = c;

                var flow = math.lerp(math.lerp(a, b, uv.x), math.lerp(c, d, uv.x), uv.y);
                float2 moveVector = flow ;
                moveVector += random.NextFloat2Direction() * 0.002f;
                int2 newTile = new int2((int) math.floor(pos.x + moveVector.x + .5f), (int) math.floor(pos.y + .5f));
                if (mapData.IsWall(newTile))
                {
                    moveVector.x = 0f;
                }

                newTile = new int2((int) math.floor(pos.x + .5f), (int) math.floor(pos.y + moveVector.y + .5f));
                if (mapData.IsWall(newTile))
                {
                    moveVector.y = 0f;
                }

                float3 moveVector3 = math.normalize(new float3(moveVector.x+agent.avoidanceVector.x, 0, moveVector.y+agent.avoidanceVector.z));
                if (math.lengthsq(moveVector3) > 0f)
                {
                    quaternion smoothedRotation = math.slerp(rotation.Value,
                        quaternion.LookRotationSafe(moveVector3, math.up()),
                        1f - math.exp(-agent.orientSharpness * deltaTime));
                    rotation.Value = smoothedRotation;
                }


                float3 targetPlanarVel = math.mul(rotation.Value, new float3(0, 0, 1)) * agent.moveSpeed * agent.obstacleDistanceFactor;
                
                velocity.Linear = math.lerp(velocity.Linear, targetPlanarVel,
                                 1f - math.exp(-agent.moveSharpness*10 * deltaTime)) + agent.storedImpulse / agent.decollisionDamping;
                //float3 vel = targetPlanarVel + agent.storedImpulse / agent.decollisionDamping;
                velocity.Linear.y = 0;
                agent.storedImpulse = default;
                
                //velocity.Linear.y = 0; 
                
                //translation.Value += vel * deltaTime;

            }
        }
    }
}