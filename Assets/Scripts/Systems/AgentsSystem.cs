using FlowFields.Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace FlowFields.Systems
{
    //[UpdateInGroup(typeof(InitializationSystemGroup))]
    [UpdateBefore(typeof(AgentMoveSystem))]
    public partial class AgentsSystem:SystemBase
    {
        public NativeMultiHashMap<int2, Translation> agentsPositions;
        
        protected override void OnUpdate()
        {
            if (!agentsPositions.IsCreated)
            {
                var map = GetSingleton<Map>();
                agentsPositions = new NativeMultiHashMap<int2, Translation>(map.width * map.height, Allocator.Persistent);
            }
            agentsPositions.Clear();

            Dependency = new UpdateAgentsSectorJob().ScheduleParallel(Dependency);
            Dependency = new AddAgentsTuBucketsJob
            {
                agents = agentsPositions.AsParallelWriter()
            }.ScheduleParallel(Dependency);

            Dependency = new CalculateAvoidanceVectorJob
            {
                agents = agentsPositions
            }.Schedule(Dependency);

        }
        [BurstCompile]
        private partial struct UpdateAgentsSectorJob:IJobEntity
        {
            public void Execute(ref Sector sector, in Translation translation, in NavAgent navAgent)
            {
                sector.coordinates = new int2((int) math.floor(translation.Value.x),(int) math.floor(translation.Value.z));
            }
        }
        [BurstCompile]
        private partial struct AddAgentsTuBucketsJob:IJobEntity
        {
            public NativeMultiHashMap<int2, Translation>.ParallelWriter agents;

            public void Execute(in Sector sector, in Translation translation, in NavAgent navAgent)
            {
                agents.Add(sector.coordinates, translation);
            }
        }

        [BurstCompile]
        private partial struct CalculateAvoidanceVectorJob:IJobEntity
        {
            [ReadOnly]
            public NativeMultiHashMap<int2, Translation> agents;

            public void Execute(ref NavAgent navAgent, in Translation translation, in Rotation rotation, in Sector sector)
            {

                var sqLen = navAgent.moveSpeed*navAgent.moveSpeed;
                var pos = float3.zero;
                var count = 0;
                var factor = 1.0f;

                var forward = math.mul(rotation.Value, new float3(0, 0, 1));

                if (agents.TryGetFirstValue(sector.coordinates, out var value, out var iterator))
                {

                    var vector = value.Value - translation.Value;
                    var len = math.lengthsq(vector);
                    vector = math.normalize(vector);
                    if (math.dot(forward, vector)>0)
                    {
                        pos = vector * (1 - math.clamp(len / sqLen, 0, 1));
                        count++;
                    }

                    while (agents.TryGetNextValue(out value, ref iterator))
                    {
                        vector = value.Value - translation.Value;
                        len = math.lengthsq(vector);
                        if (math.dot(forward, vector) > 0.3f)
                        {
                            var f = math.clamp(len / sqLen, 0, 1);
                            factor = math.min(factor, f);
                            pos += math.normalize(vector) * (1 - f);
                            count++;
                        }
                    }
                }

                if (count > 0)
                    pos = -pos/count;
                navAgent.obstacleDistanceFactor = factor;
                navAgent.avoidanceVector = pos;


            }
        }
    }
}