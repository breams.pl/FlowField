using FlowFields.Components;
using Unity.Entities;

namespace FlowFields.Systems
{
    public partial class AnimationSystem:SystemBase
    {
        protected override void OnUpdate()
        {
            var time = Time.DeltaTime;
            var animationOffset = 10000;
            var frameOffset = 100;
            Entities.ForEach((ref Animation animation, ref VerticalOffset offset) =>
            {
                animation.CurrentTime += time;
                if (animation.CurrentTime > animation.TimePerFrame)
                {

                    offset.Value = -(animation.AnimationId * animationOffset + animation.FrameIndex * frameOffset);
                    animation.CurrentTime -= animation.TimePerFrame;
                    animation.FrameIndex++;
                    if (animation.FrameIndex == animation.FramesCount)
                        animation.FrameIndex = 0;
                }

            }).ScheduleParallel();
        }
    }
}