using FlowFields.Components;
using FlowFields.Data;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.UI;
using Random = Unity.Mathematics.Random;

namespace FlowFields.Systems
{
    [UpdateInGroup(typeof(InitializationSystemGroup))]
    //[AlwaysUpdateSystem]
    public partial class MapGenerationSystem : SystemBase
    {
        public MapData mapData;
        //public JobHandle generateMapHandle;
        private EndInitializationEntityCommandBufferSystem _barier;
        private Random random;
        private EntityQuery query;
        private EntityQuery queryTiles;
        private EntityArchetype tileArchetype;

        protected override void OnCreate()
        {
            base.OnCreate();
            _barier = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<EndInitializationEntityCommandBufferSystem>();
            EntityQueryDesc desc = new EntityQueryDesc()
            {
                All = new[] {ComponentType.ReadWrite<Map>()}
            };
            query = EntityManager.CreateEntityQuery(desc);
            EntityQueryDesc tilesDesc = new EntityQueryDesc()
            {
                All = new[] {ComponentType.ReadWrite<MapTile>()}
            };
            queryTiles = EntityManager.CreateEntityQuery(tilesDesc);
            tileArchetype = EntityManager.CreateArchetype(ComponentType.ReadWrite<MapTile>());
            random = new Random(42);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            mapData.Dispose();
        }
        protected override void OnUpdate()
        {
            if (mapData.IsValid)
            {
                return;
            }

            var maps = query.ToComponentDataArray<Map>(Allocator.TempJob);
            if (maps.Length == 0)
            {
                maps.Dispose();
                return;
            }
                
            var map = maps[0];
            maps.Dispose();
            mapData = new MapData(map.width, map.height);

            var updateMap = new GenerateMapJob()
            {
                mapTiles =  mapData.tiles,
                width = mapData.width
            };
            Dependency = updateMap.Schedule(Dependency);
            var commandBuffer = _barier.CreateCommandBuffer();
            var directions = new GenerateDirectionsJob()
            {
                map = mapData,
                CommandBuffer = commandBuffer.AsParallelWriter()
            };
            Dependency = directions.Schedule(Dependency);
            //inputDeps = directions.Schedule(this, inputDeps);

            _barier.AddJobHandleForProducer(Dependency);
            /*
            if (map.width <= 0 || map.height <= 0)
                return inputDeps;
           var tiles = queryTiles.ToComponentDataArray<MapTile>(Allocator.TempJob);
           /*
            var commandBuffer = _barier.CreateCommandBuffer();
            var tilesJob = new GenerateTilesJob()
            {
                tileArchetype = tileArchetype,
                tiles = tiles,
                width = map.width,
                height = map.height,
                random = random,
                commandBuffer = commandBuffer
            };

            
           //var handle = tilesJob.Schedule(inputDeps);
           // _barier.AddJobHandleForProducer(handle);
           mapData = new MapData(map.width, map.height);
            var mapJob = new GenerateMapJob()
            {
                tiles = tiles,
                mapTiles = mapData.tiles,
                width = map.width
            };

            generateMapHandle = mapJob.Schedule(inputDeps);

            return generateMapHandle;*/
        }

        private partial struct GenerateDirectionsJob : IJobEntity
        {
            [ReadOnly]
            public MapData map;
            public EntityCommandBuffer.ParallelWriter CommandBuffer;
            public void Execute(Entity entity, [EntityInQueryIndex] int index, ref MapTile mapTile, in  Sector sector)
            {
                DynamicBuffer<DirectionBufferElement> buffer =
                    CommandBuffer.AddBuffer<DirectionBufferElement>(index, entity);

                //Reinterpret to plain int buffer
                DynamicBuffer<int2> int2Buffer = buffer.Reinterpret<int2>();
                var x = sector.coordinates.x;
                var y = sector.coordinates.y;
                var l = 8;//map.MoveDirsIndexOffset.Length;
                byte code = 0;
                byte one = 1;
                if (map.IsWall(sector.coordinates))
                {
                    mapTile.availableDirectionsCode = byte.MaxValue;
                    return;
                }
                for (var i = 0; i < l; i++)
                {
                    var moveDir = map.mapDataBlob.Value.MoveDirections[i];
                    var newTile = new int2(x + moveDir.x, y + moveDir.y);
                    if (!map.IsInsideMap(newTile) || map.IsWall(newTile))
                    {
                        code += (byte) (one << i);
                    }
                }
/*
                for (int i = 0, last = l-2 ; i < l; i+=2)
                {
                    var moveDirTested = map.MoveDirs[i+1];
                    var testedTile = new int2(x + moveDirTested.x, y + moveDirTested.y);
                    if(!map.IsInsideMap(testedTile) || map.IsWall(testedTile))
                        continue;

                    var moveDir = map.MoveDirs[i];
                    var newTile = new int2(x + moveDir.x, y + moveDir.y);
                    var moveDir2= i == last ?  map.MoveDirs[0] : map.MoveDirs[i+2];
                    var newTile2 = new int2(x + moveDir2.x, y + moveDir2.y);
                    if ((!map.IsInsideMap(newTile) || map.IsWall(newTile)) || (!map.IsInsideMap(newTile2) || map.IsWall(newTile2)))
                    {
                        code += (byte) (one << (i+1));
                    }
                }*/
                //for (var i = 0; i < l; i++)
                //{
                //    var hasWall = (code & (one << i)) != 0;
                //    if (!hasWall)
                //        int2Buffer.Add(map.MoveDirs[i]);
                //}

                mapTile.availableDirectionsCode = code;
            }
        }
        [BurstCompile]
        private partial struct GenerateMapJob : IJobEntity
        {
            [NativeDisableParallelForRestriction]
            public NativeArray<MapTile> mapTiles;

            public int width;


            public void Execute(ref MapTile c0, ref Sector c1)
            {
                mapTiles[c1.coordinates.x + c1.coordinates.y * width] = c0;
            }
        }
/*
        //[BurstCompile]
        private struct GenerateTilesJob : IJob
        {
            [ReadOnly] public EntityArchetype tileArchetype;
            [ReadOnly] public int width;
            [ReadOnly] public int height;
            [ReadOnly] public Random random;

            public NativeArray<MapTile> tiles;
            public EntityCommandBuffer commandBuffer;

            public void Execute()
            {
                for (var x = 0; x < width; x++)
                for (var y = 0; y < height; y++)
                {
                    var tile = new MapTile
                    {
                        position = new int2(x, y),
                        moveCost = (byte) random.NextInt(1, 8)
                    };

                    var entity = commandBuffer.CreateEntity(tileArchetype);
                    commandBuffer.SetComponent(entity, tile);
                    tiles[x + y * width] = tile;
                }
            }
        }

        [BurstCompile]
        private struct GenerateMapJob : IJob
        {
            [DeallocateOnJobCompletion] [ReadOnly] public NativeArray<MapTile> tiles;
            [ReadOnly] public int width;

            public NativeArray<MapTile> mapTiles;

            public void Execute()
            {
                for (var i = 0; i < tiles.Length; i++)
                {
                    var tile = tiles[i];
                    var position = tile.position;
                    mapTiles[position.x + position.y * width] = tile;
                }
            }
        }
        */
    }
}