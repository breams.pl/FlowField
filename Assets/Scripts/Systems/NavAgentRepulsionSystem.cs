using FlowFields.Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;

namespace FlowFields.Systems
{
    /*
    [UpdateAfter(typeof(EndFramePhysicsSystem))]
    public partial class NavAgentCollisionsSystem : JobComponentSystem
    {
        private BuildPhysicsWorld buildPhysicsWorldSystem;
        private StepPhysicsWorld stepPhysicsWorldSystem;

        [BurstCompile]
        public struct CharacterRepulsionJob : ITriggerEventsJob
        {
            [ReadOnly] public ComponentDataFromEntity<Translation> TranslationGroup;
            public ComponentDataFromEntity<NavAgent> navAgentsGroup;

            public void Execute(TriggerEvent collisionEvent)
            {
                Entity entityA = collisionEvent.Entities.EntityA;
                Entity entityB = collisionEvent.Entities.EntityB;
                bool entityAIsCharacter = navAgentsGroup.HasComponent(entityA);
                bool entityBIsCharacter = navAgentsGroup.HasComponent(entityB);

                if (entityAIsCharacter || entityBIsCharacter)
                {
                    float3 offsetBToA = TranslationGroup[entityA].Value - TranslationGroup[entityB].Value;

                    if (entityAIsCharacter)
                    {
                        NavAgent characterA = navAgentsGroup[entityA];
                        characterA.storedImpulse += offsetBToA;
                        navAgentsGroup[entityA] = characterA;
                    }

                    if (entityBIsCharacter)
                    {
                        NavAgent characterB = navAgentsGroup[entityB];
                        characterB.storedImpulse -= offsetBToA;
                        navAgentsGroup[entityB] = characterB;
                    }

                }
            }
        }

        protected override void OnCreate()
        {
            base.OnCreate();

            buildPhysicsWorldSystem = World.GetOrCreateSystem<BuildPhysicsWorld>();
            stepPhysicsWorldSystem = World.GetOrCreateSystem<StepPhysicsWorld>();
        }

        protected override JobHandle OnUpdate(JobHandle inputDependencies)
        {
            CharacterRepulsionJob characterRepulsionJob = new CharacterRepulsionJob();
            characterRepulsionJob.TranslationGroup = GetComponentDataFromEntity<Translation>(true);
            characterRepulsionJob.navAgentsGroup = GetComponentDataFromEntity<NavAgent>();
            inputDependencies = characterRepulsionJob.Schedule(stepPhysicsWorldSystem.Simulation,
                ref buildPhysicsWorldSystem.PhysicsWorld, inputDependencies);

            return inputDependencies;
        }
    }*/
}