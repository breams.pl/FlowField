using FlowFields.Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.UI;
using Random = Unity.Mathematics.Random;

namespace FlowFields.Systems
{
    public partial  class SpawningSystem:SystemBase
    {

        private Random random;
        private BeginSimulationEntityCommandBufferSystem barier;
        
        protected override void OnCreate()
        {
            base.OnCreate();

            barier = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
            random = new Random(43);
        }

        protected override void OnUpdate()
        {
            //var prefabsToSpawn = new NativeList<SpawnPrefab>(Allocator.TempJob);
            var ticker = new TickSpawnerJob
            {
                deltaTime = Time.DeltaTime,
                //prefabsToSpawn = prefabsToSpawn,
                random = random,
                commandBuffer = barier.CreateCommandBuffer().AsParallelWriter()
            };
            Dependency = ticker.Schedule(Dependency);
            
            /*
            var spawner = new SpawnPrefabs
            {
                prefabsToSpawn = prefabsToSpawn,
                random = random,
                commandBuffer = barier.CreateCommandBuffer().ToConcurrent()
            };
            inputDeps = spawner.Schedule(prefabsToSpawn.Length,64, inputDeps);*/

            barier.AddJobHandleForProducer(Dependency);
        }
        
        //[BurstCompile]
        private partial struct TickSpawnerJob : IJobEntity
        {
            [ReadOnly]
            public float deltaTime;
            //public NativeList<SpawnPrefab> prefabsToSpawn;
            public Random random;
            public EntityCommandBuffer.ParallelWriter commandBuffer;
            
            public void Execute(Entity entity, [EntityInQueryIndex] int index, ref Spawner spawner, in MapTile tile, in Sector sector)
            {
                spawner.time -= deltaTime;
                if (spawner.time <= 0)
                {
                    spawner.time += spawner.timeToSpawn;
                    
                    var e = commandBuffer.Instantiate(index, spawner.prefab);
                    var rand = random.NextFloat2Direction() * 0.5f;
                    float3 position = new float3(sector.coordinates.x + rand.x, 0.0f , sector.coordinates.y + rand.y);
                    commandBuffer.SetComponent(index, e, new Translation { Value = position});
                    var color = new float4(random.NextInt(0,2),1,1,1);
                    commandBuffer.SetComponent(index, e, new MaterialColor(){ Value = color});
                    commandBuffer.SetComponent(index, e, new VerticalOffset(){ Value = random.NextInt(0,10)});
                    commandBuffer.AddComponent(index, e, new Sector { coordinates = sector.coordinates});
                }
            }
        }

    }
}