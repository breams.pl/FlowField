using System.Text;
using FlowFields.Components;
using FlowFields.Data;
using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.PerformanceTesting;
using UnityEngine;

namespace Tests.Editor
{
    public class AStarTest
    {
        private const float MOVE_STRAIGHT_COST = 1;
        private const float MOVE_DIAGONAL_COST = math.SQRT2;
        private readonly int iterations = 5;
        private readonly int mapSize = 100;
        private readonly int measure = 50;
        private readonly int warmup = 10;
        private AStarField field;
        private MapData map;
        private int2 endPosition;
        [Test]
        [Performance]
        public void CurrentSolution()
        {
            Setup();


            var openList = new NativeList<NextNode>(Allocator.TempJob);
            var closedListFlag = new NativeArray<bool>(mapSize*mapSize, Allocator.TempJob);

            Measure.Method(() =>
                {

                    openList.Clear();

                    var resetJob = new ResetField
                    {
                        nodes = field.Nodes,
                        endPosition = endPosition,
                        startIndex = 0
                    };
                    var resetFlagsJob = new ResetFlags
                    {
                        closedListFlag = closedListFlag
                    };

                    var findPath = new FindPath
                    {
                        startTileIndex = 0,
                        endTileIndex = endPosition.x + endPosition.y * map.width,
                        pathNodeArray = field.Nodes,
                        openList = openList,
                        closedList = closedListFlag,
                        blob = map.mapDataBlob
                    };
                    var handle = resetJob.Schedule(field.Nodes.Length, 64);
                    var handle2 = resetFlagsJob.Schedule(field.Nodes.Length, 64);
                    handle = JobHandle.CombineDependencies(handle, handle2);

                    handle = findPath.Schedule(handle);
                    handle.Complete();
                })
                .WarmupCount(warmup)
                .MeasurementCount(measure)
                .IterationsPerMeasurement(iterations)
                .GC()
                .Run();
            closedListFlag.Dispose();
            openList.Dispose();
            TearDown();
        }

        private void Setup()
        {
            map = new MapData(mapSize, mapSize);
            endPosition = new int2(map.width - 1, map.height - 1);
            for (var x = 0; x < mapSize; x++)
            for (var y = 0; y < mapSize; y++)
            {
                byte code = 0;
                map.SetMapTile(new int2(x, y), new MapTile
                {
                    agents = 0,
                    availableDirectionsCode = code,
                    moveCost = (byte) (x > 0 && y == mapSize/2 ? 8 : 1)
                });
            }

            for (var x = 0; x < mapSize; x++)
            for (var y = 0; y < mapSize; y++)
            {
                byte code = 0;
                byte one = 1;
                for (var i = 0; i < 8; i++)
                {
                    var moveDir = map.mapDataBlob.Value.MoveDirections[i];
                    var newTile = new int2(x + moveDir.x, y + moveDir.y);
                    if (!map.IsInsideMap(newTile) || map.IsWall(newTile)) code += (byte) (one << i);
                }

                map.SetMapTile(new int2(x, y), new MapTile
                {
                    agents = 0,
                    availableDirectionsCode = code,
                    moveCost = (byte) (x > 0 && y == mapSize/2 ? 8 : 1)
                });
            }

            field = new AStarField(map);
        }


        private void TearDown()
        {
            var sb = new StringBuilder();
            for (var y = mapSize-1; y >=0; y--)
            {
                for (var x = 0; x < mapSize; x++)
                {
                    sb.Append(GetCharForCords(x, y));
                }

                sb.Append("\n");
            }
            Debug.Log(sb.ToString());
;
            map.Dispose();
            field.Dispose();
        }
        private char GetCharForCords(int x, int y)
        {
            var node = field.Nodes[x + y * mapSize];
            if (map.IsWall(new int2(x,y)))
                return 'W';
            if (node.cameFromNodeIndex == -1)
                return 'M';

            var diff = node.cameFromNodeIndex-node.index;
            switch (diff)
            {
                case 100:
                    return '↓';
                case 101:
                    return '↙';
                case 1:
                    return '←';
                case -99:
                    return '↖';
                case -100:
                    return '↑';
                case -101:
                    return '↗';
                case -1:
                    return '→';
                case 99:
                    return '↘';
                default:
                    return 'D';
            }
        }
        [BurstCompile]
        private struct ResetFlags : IJobParallelFor
        {
            //public NativeArray<bool> openListFlag;
            public NativeArray<bool> closedListFlag;

            public void Execute(int index)
            {
              //  openListFlag[index] = false;
                closedListFlag[index] = false;
            }

        }
        [BurstCompile]
        private struct ResetField : IJobParallelFor
        {
            public NativeArray<PathNode> nodes;
            [ReadOnly]
            public int2 endPosition;
            [ReadOnly]
            public int startIndex;
            public void Execute(int index)
            {
                var node = nodes[index];
                node.fCost = 0;
                node.hCost = CalculateDistanceCost(node.position, endPosition);
                node.gCost = math.select(float.MaxValue, 0, index == startIndex);
                node.cameFromNodeIndex = -1;

                nodes[index] = node;
            }

            private float CalculateDistanceCost(int2 aPosition, int2 bPosition)
            {
                var xDistance = math.abs(aPosition.x - bPosition.x);
                var yDistance = math.abs(aPosition.y - bPosition.y);
                var remaining = math.abs(xDistance - yDistance);
                return MOVE_DIAGONAL_COST * math.min(xDistance, yDistance) + MOVE_STRAIGHT_COST * remaining;
            }
        }

        [BurstCompile]
        private struct FindPath : IJob
        {
            [ReadOnly] public int startTileIndex;
            [ReadOnly] public int endTileIndex;
            public NativeArray<PathNode> pathNodeArray;
            public NativeList<NextNode> openList;
            public NativeArray<bool> closedList;
            [ReadOnly] public BlobAssetReference<MapDataBlob> blob;


            public void Execute()
            {
                openList.Add(new NextNode
                {
                    index = startTileIndex,
                    codeModifier = 0
                });

                while (openList.Length > 0)
                {
                    var currentOpen = GetLowestCostFNodeIndex(openList);
                    var currentNodeIndex = currentOpen.index;
                    var currentNode = pathNodeArray[currentNodeIndex];

                    if (currentNodeIndex == endTileIndex) break;

                    for (var i = 0; i < openList.Length; i++)
                    {
                        if (openList[i].index != currentNodeIndex) continue;
                        openList.RemoveAtSwapBack(i);
                    }

                    closedList[currentNodeIndex] = true;
                    var code = (byte) (currentNode.DirectionCode | currentOpen.codeModifier);
                    var dirs = blob.Value.CodeStartEndIndex[code];

                    for (var i = dirs.startIndex; i < dirs.endIndex; i++)
                    {
                        var directionData = blob.Value.DirectionData[i];
                        var neighbourNodeIndex = currentNodeIndex + directionData.MoveDirectionIndexOffset;
                        if (closedList[neighbourNodeIndex]) continue;
                        var neighbourNode = pathNodeArray[neighbourNodeIndex];
                        var tentativeGCost = currentNode.gCost + directionData.DirectionLength;
                        if (tentativeGCost < neighbourNode.gCost)
                        {
                            neighbourNode.cameFromNodeIndex = currentNodeIndex;
                            neighbourNode.gCost = tentativeGCost;
                            neighbourNode.CalculateFCost();
                            pathNodeArray[neighbourNodeIndex] = neighbourNode;

                            //if (!OpenListContainsIndex(neighbourNodeIndex))
                                openList.Add(new NextNode
                                {
                                    index = neighbourNodeIndex,
                                    codeModifier = directionData.CodeModifications,
                                    cost = neighbourNode.fCost
                                });
                        }
                    }
                }
            }

            private bool OpenListContainsIndex(int index)
            {
                for (var i = 0; i < openList.Length; i++)
                {
                    var node = openList[i];
                    if (node.index == index)
                        return true;
                }

                return false;
            }

            private float CalculateDistanceCost(int2 aPosition, int2 bPosition)
            {
                var xDistance = math.abs(aPosition.x - bPosition.x);
                var yDistance = math.abs(aPosition.y - bPosition.y);
                var remaining = math.abs(xDistance - yDistance);
                return MOVE_DIAGONAL_COST * math.min(xDistance, yDistance) + MOVE_STRAIGHT_COST * remaining;
            }

            private NextNode GetLowestCostFNodeIndex(NativeList<NextNode> openList)
            {

                var lowestCostPathNode = openList[0];

                for (var i = 1; i < openList.Length; i++)
                {
                    var testPathNode = openList[i];
                    if (testPathNode.cost > lowestCostPathNode.cost) continue;
                    lowestCostPathNode = testPathNode;
                }

                return lowestCostPathNode;
            }
        }

        public struct NextNode
        {
            public int index;
            public byte codeModifier;
            public float cost;
        }

    }
}