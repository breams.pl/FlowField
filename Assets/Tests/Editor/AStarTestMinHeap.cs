using System.Text;
using BovineLabs.Systems.Pathfinding;
using FlowFields.Components;
using FlowFields.Data;
using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.PerformanceTesting;
using UnityEngine;

namespace Tests.Editor
{
    public class AStarTestMinHeap
    {
        private readonly int iterations = 1;
        private readonly int mapSize = 1000;
        private readonly int measure = 10;
        private readonly int warmup = 1;
        private int2 endPosition;
        private AStarField field;
        private MapData map;

        [Test]
        [Performance]
        public void CurrentSolution()
        {
            Setup();
            var openList = new NativeMinHeap(map.height * map.width * 10, Allocator.TempJob);
            var openListFlag = new NativeArray<bool>(map.height * map.width, Allocator.TempJob);
            var closedListFlag = new NativeArray<bool>(map.height * map.width, Allocator.TempJob);
            Measure.Method(() =>
                {
                    openList.Clear();
                    var resetFlagsJob = new ResetFlags
                    {
                        openListFlag = openListFlag,
                        closedListFlag = closedListFlag
                    };

                    var resetJob = new ResetField
                    {
                        nodes = field.Nodes,
                        endPosition = endPosition,
                        startIndex = 0
                    };

                    var findPath = new FindPath
                    {
                        startTileIndex = 0,
                        endTileIndex = endPosition.x + endPosition.y * map.width,
                        pathNodeArray = field.Nodes,
                        openList = openList,
                        openListFlag = openListFlag,
                        closedListFlag = closedListFlag,
                        blob = map.mapDataBlob
                    };

                    var handle = resetJob.Schedule(field.Nodes.Length, 64);
                    var handle2 = resetFlagsJob.Schedule(field.Nodes.Length, 64);
                    handle = JobHandle.CombineDependencies(handle, handle2);
                    handle = findPath.Schedule(handle);
                    handle.Complete();
                })
                .WarmupCount(warmup)
                .MeasurementCount(measure)
                .IterationsPerMeasurement(iterations)
                .GC()
                .Run();
            openList.Dispose();
            openListFlag.Dispose();
            closedListFlag.Dispose();
            TearDown();
        }

        private void Setup()
        {
            map = new MapData(mapSize, mapSize);
            endPosition = new int2(map.width - 1, map.height - 1);
            for (var x = 0; x < mapSize; x++)
            for (var y = 0; y < mapSize; y++)
            {
                byte code = 0;
                map.SetMapTile(new int2(x, y), new MapTile
                {
                    agents = 0,
                    availableDirectionsCode = code,
                    moveCost = (byte) (x > 0 && y == mapSize / 2 ? 8 : 1)
                });
            }

            for (var x = 0; x < mapSize; x++)
            for (var y = 0; y < mapSize; y++)
            {
                byte code = 0;
                byte one = 1;
                for (var i = 0; i < 8; i++)
                {
                    var moveDir = map.mapDataBlob.Value.MoveDirections[i];
                    var newTile = new int2(x + moveDir.x, y + moveDir.y);
                    if (!map.IsInsideMap(newTile) || map.IsWall(newTile)) code += (byte) (one << i);
                }

                map.SetMapTile(new int2(x, y), new MapTile
                {
                    agents = 0,
                    availableDirectionsCode = code,
                    moveCost = (byte) (x > 0 && y == mapSize / 2 ? 8 : 1)
                });
            }
            field = new AStarField(map);
        }


        private void TearDown()
        {
            var sb = new StringBuilder();
            for (var y = mapSize-1; y >=0; y--)
            {
                for (var x = 0; x < mapSize; x++)
                {
                    sb.Append(GetCharForCords(x, y));
                }

                sb.Append("\n");
            }
            Debug.Log(sb.ToString());

            Debug.Log(sb.ToString());
            map.Dispose();
            field.Dispose();
        }

        private char GetCharForCords(int x, int y)
        {
            var node = field.Nodes[x + y * mapSize];
            if (map.IsWall(new int2(x,y)))
                return 'W';
            if (node.cameFromNodeIndex == -1)
                return 'M';

            var diff = node.cameFromNodeIndex-node.index;
            switch (diff)
            {
                    case 100:
                        return '↓';
                    case 101:
                        return '↙';
                    case 1:
                        return '←';
                    case -99:
                        return '↖';
                    case -100:
                        return '↑';
                    case -101:
                        return '↗';
                    case -1:
                        return '→';
                    case 99:
                        return '↘';
                    default:
                        return 'D';
            }
        }
        [BurstCompile]
        private struct ResetFlags : IJobParallelFor
        {
            public NativeArray<bool> openListFlag;
            public NativeArray<bool> closedListFlag;

            public void Execute(int index)
            {
                openListFlag[index] = false;
                closedListFlag[index] = false;
            }
        }

        [BurstCompile]
        private struct ResetField : IJobParallelFor
        {
            public NativeArray<PathNode> nodes;
            [ReadOnly] public int2 endPosition;
            [ReadOnly] public int startIndex;

            public void Execute(int index)
            {
                var node = nodes[index];
                node.fCost = 0;
                node.hCost = AStarField.CalculateDistanceCost(node.position, endPosition);
                node.gCost = math.select(float.MaxValue, 0, index == startIndex);
                node.cameFromNodeIndex = -1;
                nodes[index] = node;
            }
        }

        [BurstCompile]
        private struct FindPath : IJob
        {
            [ReadOnly] public int startTileIndex;
            [ReadOnly] public int endTileIndex;
            public NativeArray<PathNode> pathNodeArray;
            public NativeArray<bool> openListFlag;
            public NativeArray<bool> closedListFlag;
            public NativeMinHeap openList;
            [ReadOnly] public BlobAssetReference<MapDataBlob> blob;


            public void Execute()
            {
                openList.Push(new MinHeapNode(startTileIndex, 0, 0));
                closedListFlag[startTileIndex] = true;
                while (openList.HasNext())
                {
                    var heapIndex = openList.Pop();
                    var currentOpen = openList[heapIndex];
                    var currentNodeIndex = currentOpen.Index;
                    if (currentNodeIndex == endTileIndex)
                        break;
                    openListFlag[currentNodeIndex] = false;
                    var currentNode = pathNodeArray[currentNodeIndex];
                    var code = (byte) (currentNode.DirectionCode | currentOpen.CodeModifier);
                    var dirs = blob.Value.CodeStartEndIndex[code];
                    closedListFlag[currentNodeIndex] = true;
                    for (var i = dirs.startIndex; i < dirs.endIndex; i++)
                    {
                        var directionData = blob.Value.DirectionData[i];
                        var neighbourNodeIndex = currentNodeIndex + directionData.MoveDirectionIndexOffset;
                        if (closedListFlag[neighbourNodeIndex]) continue;
                        var neighbourNode = pathNodeArray[neighbourNodeIndex];
                        var tentativeGCost = currentNode.gCost + directionData.DirectionLength;

                        if (tentativeGCost > neighbourNode.gCost) continue;

                        neighbourNode.cameFromNodeIndex = currentNodeIndex;
                        neighbourNode.gCost = tentativeGCost;
                        neighbourNode.CalculateFCost();
                        pathNodeArray[neighbourNodeIndex] = neighbourNode;

                        if (openListFlag[neighbourNodeIndex]) continue;

                        openList.Push(new MinHeapNode(neighbourNodeIndex, neighbourNode.fCost,
                            directionData.CodeModifications));
                        openListFlag[neighbourNodeIndex] = true;
                    }
                }
            }
        }
    }
}