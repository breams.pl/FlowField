using System.Collections.Generic;
using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.PerformanceTesting;
using UnityEngine;
using Random = Unity.Mathematics.Random;

namespace Tests.Editor
{
    public class BigSpreading
    {
        private NativeArray<float> _BaseArray;
        private NativeArray<float> _BaseArray1;
        private NativeArray<float> _BaseArray2;
        private NativeArray<float> _ResultArray;
        private NativeArray<Data> _DataArray;
        private List<DataClass> _DataClassArray;
        private List<DataClass>[] _DataClassArrays;
        private List<DataClass> _DataClassArrays2;
        private List<DataClass> _DataClassArrays3;
        private ResultClass _Result = new ResultClass();
        private int _Size = 1024 * 200;
        private Random _R;
        private int warmup = 10;
        private int measure = 10;
        private int iterations = 5;
        public void Setup(bool memorySetup = false)
        {
            _R = new Random(42);
            _BaseArray = new NativeArray<float>(_Size, Allocator.Persistent);
            _BaseArray1 = new NativeArray<float>(_Size, Allocator.Persistent);
            _BaseArray2 = new NativeArray<float>(_Size, Allocator.Persistent);
            _ResultArray = new NativeArray<float>(_Size, Allocator.Persistent);
            _DataArray = new NativeArray<Data>(_Size, Allocator.Persistent);
            if (memorySetup)
            {
                _DataClassArrays = new List<DataClass>[10];
                for (int d = 0; d < 10; d++)
                {
                    var l = new List<DataClass>();
                    for (int i = 0; i < _DataArray.Length; i++)
                    {
                        l.Add(new DataClass()
                        {
                            Value1 = _DataArray[i].Value1,
                            Value2 = _DataArray[i].Value2,
                        });
                    }

                    _DataClassArrays[d] = l;
                }

                for (int d = 0; d < 10; d++)
                {
                    var l = _DataClassArrays[d];
                    for (int i = l.Count - 1; i > 0; i--)
                    {
                        if (_R.NextFloat() > 0.700f)
                            l.RemoveAt(i);
                    }
                }
            }

            _DataClassArray = new List<DataClass>();
            for (int i = 0; i < _DataArray.Length; i++)
            {
                _DataClassArray.Add(new DataClass()
                {
                    Value1 = _DataArray[i].Value1,
                    Value2 = _DataArray[i].Value2,
                });
            }
        }

        public void TearDown()
        {
            _BaseArray.Dispose();
            _BaseArray1.Dispose();
            _BaseArray2.Dispose();
            _ResultArray.Dispose();
            _DataArray.Dispose();
        }
        [Test, Performance]
        public void Native()
        {
            Setup();
            Measure.Method(() =>
                {
                    var c = 0f;
                    for (int i = 0, count = _DataArray.Length; i < count; i++)
                    {
                        var v = _DataArray[i];
                        c += v.Value1 + v.Value2;
                    }
                })
                .WarmupCount(warmup)
                .MeasurementCount(measure)
                .IterationsPerMeasurement(iterations)
                .GC()
                .Run();
            TearDown();
        }

        [Test, Performance]
        public void List()
        {
            Setup(true);
            Measure.Method(() =>
                {
                    var c = 0f;
                    for (int i = 0, count = _DataClassArray.Count; i < count; i++)
                    {
                        var v = _DataClassArray[i];
                        c += v.Value1 + v.Value2;
                    }
                })
                .WarmupCount(warmup)
                .MeasurementCount(measure)
                .IterationsPerMeasurement(iterations)
                .GC()
                .Run();
            TearDown();
        }
        [Test, Performance]
        public void List2()
        {
            Setup(true);
            Measure.Method(() =>
                {
                    var c = 0f;
                    for (int i = 0, count = _DataClassArray.Count; i < count; i++)
                    {
                        var v = _DataClassArray[i];
                        c += v.Value1 + v.Value2;
                    }

                    _Result.Result = c;
                })
                .WarmupCount(warmup)
                .MeasurementCount(measure)
                .IterationsPerMeasurement(iterations)
                .GC()
                .Run();
            Debug.Log(_Result);
            TearDown();
        }
        [Test, Performance]
        public void List3()
        {
            Setup(true);
            Measure.Method(() =>
                {
                    for (int i = 0, count = _DataClassArray.Count; i < count; i++)
                    {
                        var v = _DataClassArray[i];
                        _Result.Result += v.Value1 + v.Value2;
                    }
                })
                .WarmupCount(warmup)
                .MeasurementCount(measure)
                .IterationsPerMeasurement(iterations)
                .GC()
                .Run();
            Debug.Log(_Result);
            TearDown();
        }
        [Test, Performance]
        public void NativeJob()
        {
            Setup();
            Measure.Method(() =>
                {
                    var job = new NativeTest()
                    {
                        data = _DataArray,
                        result = _ResultArray
                    };
                    var handle = job.Schedule(new JobHandle());
                    handle.Complete();
                })
                .WarmupCount(warmup)
                .MeasurementCount(measure)
                .IterationsPerMeasurement(iterations)
                .GC()
                .Run();
            TearDown();
        }
        [BurstCompile]
        public struct NativeTest : IJob
        {
            [ReadOnly] public NativeArray<Data> data;
            [WriteOnly] public NativeArray<float> result;
            [WriteOnly] public float c;

            public void Execute()
            {
                c = 0;
                for (int i = 0, count = data.Length; i < count; i++)
                {
                    var v = data[i];
                    c += v.Value1 + v.Value2;
                }
            }
        }
        [Test, Performance]
        public void BigLocal()
        {
            Setup();
            Measure.Method(() =>
                {
                JobHandle handle = new JobHandle();
                handle = new BigLocalJob()
                {
                    r = _R,
                    result = _ResultArray,
                    baseArray = _BaseArray,
                    baseArray1 = _BaseArray1,
                    baseArray2 = _BaseArray2
                }.Schedule(_Size, 64, handle);
                handle.Complete();
                })
                .WarmupCount(warmup)
                .MeasurementCount(measure)
                .IterationsPerMeasurement(iterations)
                .GC()
                .Run();
            TearDown();
        }
        [Test, Performance]
        public void BigGlobal()
        {
            Setup();
            Measure.Method(() =>
                {
                    JobHandle handle = new JobHandle();
                    handle = new BigGlobalJob()
                    {
                        r = _R,
                        result = _ResultArray,
                        baseArray = _BaseArray,
                        baseArray1 = _BaseArray1,
                        baseArray2 = _BaseArray2
                    }.Schedule(_Size, 64, handle);
                    handle.Complete();
                })
                .WarmupCount(warmup)
                .MeasurementCount(measure)
                .IterationsPerMeasurement(iterations)
                .GC()
                .Run();
            TearDown();
        }
        [Test, Performance]
        public void Spread()
        {
            Setup();
            Measure.Method(() =>
                {
                    JobHandle handle = new JobHandle();
                    handle = new BigSpread0Job()
                    {
                        r = _R,
                        result = _ResultArray,
                        baseArray = _BaseArray
                    }.Schedule(_Size, 64, handle);
                    handle = new BigSpread1Job()
                    {
                        r = _R,
                        result = _ResultArray,
                        baseArray1 = _BaseArray1
                    }.Schedule(_Size, 64, handle);
                    handle = new BigSpread2Job()
                    {
                        r = _R,
                        result = _ResultArray,
                        baseArray2 = _BaseArray2
                    }.Schedule(_Size, 64, handle);

                    handle.Complete();

                })
                .WarmupCount(warmup)
                .MeasurementCount(measure)
                .IterationsPerMeasurement(iterations)
                .GC()
                .Run();
            TearDown();
        }
        [BurstCompile]
        public struct BigGlobalJob : IJobParallelFor
        {
            [ReadOnly]
            public NativeArray<float> baseArray;
            public NativeArray<float> baseArray1;
            public NativeArray<float> baseArray2;
            public NativeArray<float> result;
            [ReadOnly]
            public Random r;
            public void Execute(int index)
            {
                result[index] = baseArray[index] * r.NextFloat();
                result[index] *=r.NextFloat()/baseArray1[index];
                result[index] += r.NextFloat() / baseArray2[index];
            }
        }
        [BurstCompile]
        public struct BigSpread0Job : IJobParallelFor
        {
            [ReadOnly]
            public NativeArray<float> baseArray;
            public NativeArray<float> result;
            [ReadOnly]
            public Random r;
            public void Execute(int index)
            {
                result[index] = baseArray[index] * r.NextFloat();
            }
        }
        [BurstCompile]
        public struct BigSpread1Job : IJobParallelFor
        {
            public NativeArray<float> result;
            [ReadOnly]
            public NativeArray<float> baseArray1;
            [ReadOnly]
            public Random r;
            public void Execute(int index)
            {
                result[index] *= r.NextFloat()/baseArray1[index];
            }
        }
        [BurstCompile]
        public struct BigSpread2Job : IJobParallelFor
        {
            public NativeArray<float> result;
            [ReadOnly]
            public NativeArray<float> baseArray2;
            [ReadOnly]
            public Random r;
            public void Execute(int index)
            {
                result[index] += r.NextFloat()/baseArray2[index];
            }
        }
        [BurstCompile]
        public struct BigLocalJob : IJobParallelFor
        {
            [ReadOnly]
            public NativeArray<float> baseArray;
            [ReadOnly]
            public NativeArray<float> baseArray1;
            [ReadOnly]
            public NativeArray<float> baseArray2;

            public NativeArray<float> result;
            public Random r;
            public void Execute(int index)
            {
                var loc = baseArray[index] * r.NextFloat();
                loc *= r.NextFloat()/baseArray1[index] ;
                loc += r.NextFloat()/baseArray2[index] ;

                result[index] = loc;
            }
        }


    }

    public class ResultClass
    {
        public float Result;
    }
        public struct Data
        {
            public float Value1;
            public float Value2;
        }
        public class DataClass
        {
            public float Value1;
            public float Value2;
        }
}
