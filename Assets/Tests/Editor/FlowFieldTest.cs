using System.Text;
using FlowFields.Components;
using FlowFields.Data;
using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.PerformanceTesting;
using UnityEngine;

namespace Tests.Editor
{
    public class FlowFieldTest
    {
        private readonly int iterations = 1;
        private readonly int mapSize = 1000;
        private readonly int measure = 10;
        private readonly int warmup = 1;
        private FlowField flowField;
        private MapData map;
        private int2 endPosition;
        private void Setup()
        {
            map = new MapData(mapSize, mapSize);
            endPosition = new int2(map.width - 1, map.height - 1);
            for (var x = 0; x < mapSize; x++)
            for (var y = 0; y < mapSize; y++)
            {
                byte code = 0;
                map.SetMapTile(new int2(x, y), new MapTile
                {
                    agents = 0,
                    availableDirectionsCode = code,
                    moveCost = (byte) (x > 0 && y == mapSize/2 ? 8 : 1)
                });
            }

            for (var x = 0; x < mapSize; x++)
            for (var y = 0; y < mapSize; y++)
            {
                byte code = 0;
                byte one = 1;
                for (var i = 0; i < 8; i++)
                {
                    var moveDir = map.mapDataBlob.Value.MoveDirections[i];
                    var newTile = new int2(x + moveDir.x, y + moveDir.y);
                    if (!map.IsInsideMap(newTile) || map.IsWall(newTile)) code += (byte) (one << i);
                }

                map.SetMapTile(new int2(x, y), new MapTile
                {
                    agents = 0,
                    availableDirectionsCode = code,
                    moveCost = (byte) (x > 0 && y == mapSize/2 ? 8 : 1)
                });
            }


            flowField = new FlowField(endPosition, map);
        }

        private void TearDown()
        {
            var sb = new StringBuilder();
            for (var y = mapSize-1; y >=0; y--)
            {
                for (var x = 0; x < mapSize; x++)
                {
                    var index = x + y * mapSize;
                    var flow = flowField.flowField[index];
                    var xf = flow.x;
                    var yf = flow.y;
                    if(map.IsWall(new int2(x, y)))
                            sb.Append( "W");
                    else if(Mathf.Approximately(xf,0) &&Mathf.Approximately(yf,1))
                        sb.Append( "↑");
                    else if(xf> 0 &&yf>0)
                        sb.Append( "↗");
                    else if(Mathf.Approximately(xf,1) &&Mathf.Approximately(yf,0))
                        sb.Append( "→");
                    else if(xf>0 &&yf<0)
                        sb.Append( "↘");
                    else if(Mathf.Approximately(xf,0) &&Mathf.Approximately(yf,-1))
                        sb.Append( "↓");
                    else if(xf<0 &&yf<0)
                        sb.Append( "↙");
                    else if(Mathf.Approximately(xf,-1) &&Mathf.Approximately(yf,0))
                        sb.Append( "←");
                    else if(xf<0 &&yf>0)
                        sb.Append( "↖");
                }

                sb.Append("\n");
            }
            Debug.Log(sb.ToString());

             /*UnityEngine.Debug.Log($"{flowField.stepField.Length}");
             for(int i= 0;i< flowField.stepField.Length;i++)
                 UnityEngine.Debug.Log($"{i}:{flowField.stepField[i]}");*/
            map.Dispose();
            flowField.Dispose();
        }

        [Test]
        [Performance]
        public void CurrentSolution()
        {
            Setup();


            Measure.Method(() =>
                {
                    var handle = new JobHandle();
                    var initJob = new InitializeFieldJob
                    {
                        stepField = flowField.stepField,
                        flowField = flowField.flowField,
                        target = endPosition.x+endPosition.y*mapSize
                    };
                    handle = initJob.Schedule(mapSize * mapSize, 64, handle);
                    var setTargetJob = new SetTargetJob
                    {
                        target = endPosition,
                        flowField = flowField
                    };
                    handle = setTargetJob.Schedule(handle);
                    var costFieldJob = new GenerateCostField
                    {
                        map = map,
                        blob = map.mapDataBlob,
                        stepField = flowField.stepField,
                        openSet = flowField.openSet,
                        nextSet = flowField.nextSet,
                    };
                    handle = costFieldJob.Schedule(handle);

                    var flowFieldJob = new GenerateFlowField
                    {
                        map = map,
                        blob = map.mapDataBlob,
                        stepField = flowField.stepField,
                        flowField = flowField.flowField
                    };
                    handle = flowFieldJob.Schedule(mapSize * mapSize, 64, handle);
                    handle.Complete();

                })
                .WarmupCount(warmup)
                .MeasurementCount(measure)
                .IterationsPerMeasurement(iterations)
                .GC()
                .Run();

            TearDown();
        }
        [Test]
        [Performance]
        public void NewSolution()
        {
            Setup();


            Measure.Method(() =>
                {
                    var handle = new JobHandle();
                    var initJob = new InitializeFieldJob
                    {
                        stepField = flowField.stepField,
                        flowField = flowField.flowField,
                        target = mapSize / 2 + mapSize / 2 * mapSize
                    };
                    handle = initJob.Schedule(mapSize * mapSize, 64, handle);
                    var setTargetJob = new SetTargetJob
                    {
                        target = new int2(mapSize / 2, mapSize / 2),
                        flowField = flowField
                    };
                    handle = setTargetJob.Schedule(handle);
                    var costFieldJob = new GenerateCostFieldNew
                    {
                        map = map,
                        blob = map.mapDataBlob,
                        stepField = flowField.stepField,
                        openSet = flowField.openSet,
                        nextSet = flowField.nextSet,
                    };
                    handle = costFieldJob.Schedule(handle);

                    var flowFieldJob = new GenerateFlowField
                    {
                        map = map,
                        blob = map.mapDataBlob,
                        stepField = flowField.stepField,
                        flowField = flowField.flowField
                    };
                    handle = flowFieldJob.Schedule(mapSize * mapSize, 64, handle);
                    handle.Complete();

                })
                .WarmupCount(warmup)
                .MeasurementCount(measure)
                .IterationsPerMeasurement(iterations)
                .GC()
                .Run();

            TearDown();
        }
    }

    [BurstCompile]
    public struct SetTargetJob : IJob
    {
        public int2 target;
        public FlowField flowField;

        public void Execute()
        {
            flowField.SetTarget(target);
        }
    }

    [BurstCompile]
    public struct InitializeFieldJob : IJobParallelFor
    {
        public NativeArray<float> stepField;
        public NativeArray<float2> flowField;
        public int target;

        public void Execute(int index)
        {
            stepField[index] = math.select(float.MaxValue, 0, index == target);
            flowField[index] = float2.zero;
        }
    }

    [BurstCompile(FloatPrecision.Low, FloatMode.Fast)]
    public struct GenerateCostField : IJob
    {
        [ReadOnly] public MapData map;
        [ReadOnly] public BlobAssetReference<MapDataBlob> blob;
        public NativeArray<float> stepField;
        public NativeList<NextIndex> openSet;
        public NativeList<NextIndex> nextSet;

        public void Execute()
        {
            while (openSet.Length > 0)
            {
                // repeat until out of tiles
                for (var j = 0; j < openSet.Length; j++)
                {

                    var indexWithDir = openSet[j];
                    var index = indexWithDir.index;
                    var existingCost = stepField[index];
                    var code = (byte) (map.tiles[index].availableDirectionsCode | indexWithDir.codeModifier);
                    var dirs = blob.Value.CodeStartEndIndex[code];

                    for(var i = dirs.startIndex;i<dirs.endIndex;i++)
                    {
                        var directionData = blob.Value.DirectionData[i];
                        var newIndex = index + directionData.MoveDirectionIndexOffset;
                        var mapTile = map.tiles[newIndex];
//                        var moveCost = mapTile.moveCost + mapTile.agents * 0.1f;
//                        var moveCost = mapTile.moveCost * directionData.DirectionLength +  existingCost;
                        var cost = mapTile.moveCost * directionData.DirectionLength + existingCost;
                        if (cost >= stepField[newIndex]) continue;
                        stepField[newIndex] = cost;
                        nextSet.Add(new NextIndex { index = newIndex,  codeModifier = directionData.CodeModifications});
                    }
                }

                var temp = openSet;
                openSet = nextSet;
                nextSet = temp;
                nextSet.Clear();
            }

        }
    }
    [BurstCompile(FloatPrecision.High, FloatMode.Deterministic)]
    public struct GenerateCostFieldNew : IJob
    {
        [ReadOnly] public MapData map;
        [ReadOnly] public BlobAssetReference<MapDataBlob> blob;
        public NativeArray<float> stepField;
        public NativeList<NextIndex> openSet;
        public NativeList<NextIndex> nextSet;

        public void Execute()
        {
            while (openSet.Length > 0)
            {
                // repeat until out of tiles
                for (var j = 0; j < openSet.Length; j++)
                {

                    var indexWithDir = openSet[j];
                    var index = indexWithDir.index;
                    var existingCost = stepField[index];
                    var code = (byte) (map.tiles[index].availableDirectionsCode | indexWithDir.codeModifier);
                    var dirs = blob.Value.CodeStartEndIndex[code];

                    for(var i = dirs.startIndex;i<dirs.endIndex;i++)
                    {
                        var directionData = blob.Value.DirectionData[i];
                        var newIndex = index + directionData.MoveDirectionIndexOffset;
                        var mapTile = map.tiles[newIndex];
//                        var moveCost = mapTile.moveCost + mapTile.agents * 0.1f;
//                        var moveCost = mapTile.moveCost * directionData.DirectionLength +  existingCost;
                        var cost = mapTile.moveCost * directionData.DirectionLength + existingCost;
                        if (cost >= stepField[newIndex]) continue;
                        stepField[newIndex] = cost;
                        nextSet.Add(new NextIndex { index = newIndex,  codeModifier = directionData.CodeModifications});
                    }
                }

                var temp = openSet;
                openSet = nextSet;
                nextSet = temp;
                nextSet.Clear();
            }

        }

    }

    [BurstCompile]
    public struct GenerateFlowField : IJobParallelFor
    {
        [ReadOnly] public MapData map;
        [ReadOnly] public NativeArray<float> stepField;
        public NativeArray<float2> flowField;
        [ReadOnly] public BlobAssetReference<MapDataBlob> blob;
        public void Execute(int index)
        {
            if (map.IsWall(index))
                return;
            var minNeighborCost = stepField[index];
            var bestNeighborMoveDir = new float2(0, 0);
            var valid = false;
            var dirs = blob.Value.CodeStartEndIndex[map.tiles[index]
                .availableDirectionsCode];
            for (var i = dirs.startIndex; i < dirs.endIndex; i++)
            {
                var directionData = blob.Value.DirectionData[i];
                var cost = stepField[index + directionData.MoveDirectionIndexOffset];
                var checkCost = cost < minNeighborCost;
                minNeighborCost = math.select(minNeighborCost, cost, checkCost);
                bestNeighborMoveDir = math.select(bestNeighborMoveDir, directionData.MoveDirection, checkCost);
                valid = true;
            }

            flowField[index] = math.select(float2.zero,bestNeighborMoveDir, valid);
        }
    }




}