﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Unity.PerformanceTesting;
using UnityEngine;

[Serializable]
public class SimpleTest
{

    private readonly int[,] _data = new int[1024, 1024];
    private readonly int[] _data2 = new int[2048];
    private int[] _data4 = new int[2048];
    private System.Collections.Generic.IList<int> _data3;

    [SerializeField]
    private int warmup = 10;
    [SerializeField]
    private int measure = 500;
    [SerializeField]
    private int iterations = 25;
    private void Init()
    {
        Debug.Log("Setup");
        for (int r = 0, rl = _data.GetLength(0); r < rl; r++)
        for (int c = 0, cl = _data.GetLength(1); c < cl; c++)
            _data[r, c] = 1;
    }
    private void Init2()
    {
        Debug.Log("Setup");
        for (int r = 0, rl = _data2.Length; r < rl; r++)
            _data2[r] = 1;
    }
    private void Init3()
    {
        Debug.Log("Setup");
        _data3 = new List<int>();
        for (int r = 0, rl = _data.GetLength(0); r < rl; r++)
            _data3.Add(1);
    }

    private void Init4()
    {
        Debug.Log("Setup");
        _data3 = new List<int>();
        for (int r = 0, rl = _data4.Length; r < rl; r++)
            _data4[r] = 1;
    }
    [Test]
    [Performance]
    public void ColumnFirst()
    {
        Init();
        Measure.Method(() =>
            {
                var count = 0;
                for (int c = 0, cl = _data.GetLength(1); c < cl; c++)
                for (int r = 0, rl = _data.GetLength(0); r < rl; r++)
                    count += _data[r, c];

                Debug.Log(count);
            })
            .WarmupCount(warmup)
            .MeasurementCount(measure)
            .IterationsPerMeasurement(iterations)
            .GC()
            .Run();
    }

    [Test]
    [Performance]
    public void RowFirst()
    {
        Init();
        Measure.Method(() =>
            {
                var count = 0;
                for (int r = 0, rl = _data.GetLength(0); r < rl; r++)
                for (int c = 0, cl = _data.GetLength(1); c < cl; c++)
                    count += _data[r, c];
                Debug.Log(count);
            })
            .WarmupCount(warmup)
            .MeasurementCount(measure)
            .IterationsPerMeasurement(iterations)
            .GC()
            .Run();
    }

    [Test]
    [Performance]
    public void ColumnFirst2()
    {
        Init();
        Measure.Method(() =>
            {
                var count = 0;
                for (int c = 0; c <  _data.GetLength(1); c++)
                for (int r = 0; r <  _data.GetLength(0); r++)
                    count += _data[r, c];

                Debug.Log(count);
            })
            .WarmupCount(warmup)
            .MeasurementCount(measure)
            .IterationsPerMeasurement(iterations)
            .GC()
            .Run();
    }

    [Test]
    [Performance]
    public void RowFirst2()
    {
        Init();
        Measure.Method(() =>
            {
                var count = 0;
                for (int r = 0; r < _data.GetLength(0); r++)
                for (int c = 0; c < _data.GetLength(1); c++)
                    count += _data[r, c];
                Debug.Log(count);
            })
            .WarmupCount(warmup)
            .MeasurementCount(measure)
            .IterationsPerMeasurement(iterations)
            .GC()
            .Run();
    }

    [Test]
    [Performance]
    public void Simple1ReadOnly()
    {
        Init2();
        Measure.Method(() =>
            {
                var count = 0;
                for (int r = 0, l = _data2.Length; r < l; r++)
                    count += _data2[r];
                Debug.Log(count);
            })
            .WarmupCount(warmup)
            .MeasurementCount(measure)
            .IterationsPerMeasurement(iterations)
            .GC()
            .Run();
    }

    [Test]
    [Performance]
    public void Simple2ReadOnly()
    {
        Init2();
        Measure.Method(() =>
            {
                var count = 0;
                for (int r = 0; r < _data2.Length; r++)
                    count += _data2[r];
                Debug.Log(count);
            })
            .WarmupCount(warmup)
            .MeasurementCount(measure)
            .IterationsPerMeasurement(iterations)
            .GC()
            .Run();
    }

    [Test]
    [Performance]
    public void Simple1()
    {
        Init4();
        Measure.Method(() =>
            {
                var count = 0;
                for (int r = 0, l = _data4.Length; r < l; r++)
                    count += _data4[r];
                Debug.Log(count);
            })
            .WarmupCount(warmup)
            .MeasurementCount(measure)
            .IterationsPerMeasurement(iterations)
            .GC()
            .Run();
    }

    [Test]
    [Performance]
    public void Simple2()
    {
        Init4();
        Measure.Method(() =>
            {
                var count = 0;
                for (int r = 0; r < _data4.Length; r++)
                    count += _data4[r];
                Debug.Log(count);
            })
            .WarmupCount(warmup)
            .MeasurementCount(measure)
            .IterationsPerMeasurement(iterations)
            .GC()
            .Run();
    }

    [Test]
    [Performance]
    public void SimpleList1()
    {
        Init3();
        Measure.Method(() =>
            {
                var count = 0;
                for (int r = 0, l = _data3.Count; r < l; r++)
                    count += _data3[r];
                Debug.Log(count);
            })
            .WarmupCount(warmup)
            .MeasurementCount(measure)
            .IterationsPerMeasurement(iterations)
            .GC()
            .Run();
    }

    [Test]
    [Performance]
    public void SimpleList2()
    {
        Init3();
        Measure.Method(() =>
            {
                var count = 0;
                for (int r = 0; r < _data3.Count; r++)
                    count += _data3[r];
                Debug.Log(count);
            })
            .WarmupCount(warmup)
            .MeasurementCount(measure)
            .IterationsPerMeasurement(iterations)
            .GC()
            .Run();
    }
}