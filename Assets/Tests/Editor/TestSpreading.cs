using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.PerformanceTesting;

namespace Tests.Editor
{
    public class TestSpreading
    {
        private NativeArray<float> _BaseArray;

        private NativeArray<float> _ResultArray;
        private int _Size = 1024 * 1024*4;
        private Random _R;
        private int warmup = 10;
        private int measure = 10;
        private int iterations = 5;
        public void Setup()
        {
            _R = new Random(42);
            _BaseArray = new NativeArray<float>(_Size, Allocator.Persistent);
            _ResultArray = new NativeArray<float>(_Size, Allocator.Persistent);
        }

        public void TearDown()
        {
            _BaseArray.Dispose();
            _ResultArray.Dispose();
        }
        [Test, Performance]
        public void SmallLocal()
        {
            Setup();
            Measure.Method(() =>
                {
                JobHandle handle = new JobHandle();
                handle = new SmallLocalJob()
                {
                    r = _R,
                    result = _ResultArray,
                    baseArray = _BaseArray
                }.Schedule(_Size, 64, handle);
                handle.Complete();
                })
                .WarmupCount(warmup)
                .MeasurementCount(measure)
                .IterationsPerMeasurement(iterations)
                .GC()
                .Run();
            TearDown();
        }
        [Test, Performance]
        public void SmallGlobal()
        {
            Setup();
            Measure.Method(() =>
                {
                    JobHandle handle = new JobHandle();
                    handle = new SmallGlobalJob()
                    {
                        r = _R,
                        result = _ResultArray,
                        baseArray = _BaseArray
                    }.Schedule(_Size, 64, handle);
                    handle.Complete();
                })
                .WarmupCount(warmup)
                .MeasurementCount(measure)
                .IterationsPerMeasurement(iterations)
                .GC()
                .Run();
            TearDown();
        }
        [Test, Performance]
        public void Spread()
        {
            Setup();
            Measure.Method(() =>
                {
                    JobHandle handle = new JobHandle();
                    handle = new SmallSpread0Job()
                    {
                        r = _R,
                        result = _ResultArray,
                        baseArray = _BaseArray
                    }.Schedule(_Size, 64, handle);
                    handle = new SmallSpread1Job()
                    {
                        r = _R,
                        result = _ResultArray,
                    }.Schedule(_Size, 64, handle);
                    handle = new SmallSpread2Job()
                    {
                        r = _R,
                        result = _ResultArray,
                    }.Schedule(_Size, 64, handle);

                    handle.Complete();

                })
                .WarmupCount(warmup)
                .MeasurementCount(measure)
                .IterationsPerMeasurement(iterations)
                .GC()
                .Run();
            TearDown();
        }
        [BurstCompile]
        public struct SmallGlobalJob : IJobParallelFor
        {
            [ReadOnly]
            public NativeArray<float> baseArray;
            public NativeArray<float> result;
            [ReadOnly]
            public Random r;
            public void Execute(int index)
            {
                result[index] = baseArray[index] * r.NextFloat();
                result[index] *= r.NextFloat();
                result[index] += r.NextFloat();
            }
        }
        [BurstCompile]
        public struct SmallSpread0Job : IJobParallelFor
        {
            [ReadOnly]
            public NativeArray<float> baseArray;
            public NativeArray<float> result;
            [ReadOnly]
            public Random r;
            public void Execute(int index)
            {
                result[index] = baseArray[index] * r.NextFloat();
            }
        }
        [BurstCompile]
        public struct SmallSpread1Job : IJobParallelFor
        {
            public NativeArray<float> result;
            [ReadOnly]
            public Random r;
            public void Execute(int index)
            {
                result[index] *= r.NextFloat();
            }
        }
        [BurstCompile]
        public struct SmallSpread2Job : IJobParallelFor
        {
            public NativeArray<float> result;
            [ReadOnly]
            public Random r;
            public void Execute(int index)
            {
                result[index] += r.NextFloat();
            }
        }
        [BurstCompile]
        public struct SmallLocalJob : IJobParallelFor
        {
            [ReadOnly]
            public NativeArray<float> baseArray;
            public NativeArray<float> result;
            [ReadOnly]
            public Random r;
            public void Execute(int index)
            {
                var loc = baseArray[index] * r.NextFloat();
                loc *= r.NextFloat();
                loc += r.NextFloat();

                result[index] = loc;
            }
        }


    }

}